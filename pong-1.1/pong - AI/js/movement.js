
function gameLoop(){
	
//=== Player1 movement ===\\ 
    if (up && y>0) y = y - screenHeight/70
    if (down && y<screenHeight-platform_height) y = y + screenHeight/70
    document.querySelector('.p1').style.top = y+'px'
	
//=== wall bounce ===\\ 
	if (ball_y<0){ball_dirrectionY=1;}
	if (ball_y>screenHeight-ball_size){ball_dirrectionY=2;}
	
//=== platform bounce ===\\ 
	if (ball_x<0+platform_width && ball_y>y-ball_size && ball_y<y+platform_height){
		ball_dirrectionX=1; 
		time++;
	}
	if (ball_x>screenWidth-platform_width-ball_size && ball_y>y2-ball_size && ball_y<y2+platform_height){
		ball_dirrectionX=2;
		time++;
	}
	
//=== Out of bounds ===\\ 
	if (ball_x<0-platform_width) {
		p2_points++;
		document.querySelector('.sp2').innerHTML = p2_points;
		ball_reset();
		time=0;
		speed_increase=0;
	}
	if (ball_x>screenWidth+platform_width-ball_size) {
		p1_points++;
		document.querySelector('.sp1').innerHTML = p1_points;
		ball_reset();
		time=0;
		speed_increase=0;
	}
	
//=== speed-up ===\\ 
	if (time==5) {
		speed_increase +=ball_speed/10; 
		time=0;
	}

//=== AI movement===\\ 
	if (ball_x>screenWidth/2){
		if (ball_y<y2+platform_height/2 && y2>0) y2 = y2 - screenHeight/100
		if (ball_y>y2+platform_height/2 && y2<screenHeight-platform_height) y2 = y2 + screenHeight/100
	}
	else if (y2>screenHeight/2-platform_height/2) y2 = y2 - screenHeight/100
	else if (y2<screenHeight/2-platform_height/2) y2 = y2 + screenHeight/100
	document.querySelector('.p2').style.top = y2+'px'
	
//=== Ball movement ===\\ 
	if (ball_dirrectionX==1) ball_x = ball_x+ball_speed +speed_increase
	if (ball_dirrectionX==2) ball_x = ball_x-ball_speed -speed_increase
	if (ball_dirrectionY==1) ball_y = ball_y+ball_speed +speed_increase
	if (ball_dirrectionY==2) ball_y = ball_y-ball_speed -speed_increase
	document.querySelector('.ball').style.left = ball_x+'px'
    document.querySelector('.ball').style.top = ball_y+'px'
	
//=== Ball trail ===\\ 
	if (count<=2 && count>0) count++
	if (count==0) {
		ball_x_copy = ball_x;
		ball_y_copy = ball_y;
		count++;
	}
	if (count==2) {
		sb3_x = sb2_x;
		sb3_y = sb2_y;
		sb2_x = sb1_x;
		sb2_y = sb1_y;
		sb1_x = ball_x_copy;
		sb1_y = ball_y_copy;
		count = 0;
	}
	document.querySelector('.sb1').style.left = sb1_x+'px'
    document.querySelector('.sb1').style.top = sb1_y+'px'
	document.querySelector('.sb2').style.left = sb2_x+'px'
    document.querySelector('.sb2').style.top = sb2_y+'px'
	document.querySelector('.sb3').style.left = sb3_x+'px'
	document.querySelector('.sb3').style.top = sb3_y+'px'
	

	
  window.requestAnimationFrame(gameLoop)
}
window.requestAnimationFrame(gameLoop)